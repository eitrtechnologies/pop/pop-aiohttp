import dict_tools.data
import pop.hub
import pytest


@pytest.fixture(name="hub")
def integration_hub():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="acct")
    hub.pop.sub.add(dyne_name="log")
    hub.pop.sub.add(dyne_name="tool")
    hub.pop.sub.load_subdirs(hub.tool, recurse=True)
    hub.pop.sub.add(dyne_name="exec")
    hub.pop.sub.load_subdirs(hub.exec, recurse=True)
    hub.pop.sub.add(dyne_name="server")
    hub.pop.sub.load_subdirs(hub.server, recurse=True)

    yield hub

    hub.tool.request.application.APP.freeze()
    del hub.tool.request.application.APP


@pytest.fixture()
def ctx():
    return dict_tools.data.NamespaceDict(acct={})


@pytest.fixture(scope="session")
def acct_subs():
    return ["request"]


@pytest.fixture(scope="session")
def acct_profile():
    return "test_pop_aiohttp"

async def test_delete(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.delete(ctx, url)

    # check that the request is served
    assert ret.status == 200
    assert await ret.read() == b"foobar"


async def test_get(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.get(ctx, url)

    # check that the request is served
    assert ret.status == 200
    assert await ret.read() == b"foobar"


async def test_head(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="HEAD").respond_with_data("")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.head(ctx, url)

    # check that the request is served
    assert await ret.read() == b""
    assert ret.status == 200
    assert ret.headers["Content-Type"]
    assert ret.headers["Content-Length"] == "0"


async def test_patch(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.patch(ctx, url)

    # check that the request is served
    assert ret.status == 200
    assert await ret.read() == b"foobar"


async def test_post(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.post(ctx, url)

    # check that the request is served
    assert ret.status == 200
    assert await ret.read() == b"foobar"


async def test_put(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.put(ctx, url)

    # check that the request is served
    assert ret.status == 200
    assert await ret.read() == b"foobar"


async def test_request_delete(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.request(ctx, "delete", url)

    # check that the request is served
    assert await ret.read() == b"foobar"


async def test_request_get(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.request(ctx, "get", url)

    # check that the request is served
    assert ret.status == 200
    assert await ret.read() == b"foobar"


async def test_request_head(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="HEAD").respond_with_data("")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.request(ctx, "head", url)

    # check that the request is served
    assert ret.status == 200
    assert ret.headers["Content-Type"]
    assert ret.headers["Content-Length"] == "0"
    assert await ret.read() == b""


async def test_request_patch(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.request(ctx, "patch", url)

    # check that the request is served
    assert ret.status == 200
    assert await ret.read() == b"foobar"


async def test_request_post(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.request(ctx, "post", url)

    # check that the request is served
    assert ret.status == 200
    assert await ret.read() == b"foobar"


async def test_request_put(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_data("foobar")

    url = httpserver.url_for("/foobar")
    ret = await hub.tool.request.session.request(ctx, "put", url)

    # check that the request is served
    assert ret.status == 200
    assert await ret.read() == b"foobar"

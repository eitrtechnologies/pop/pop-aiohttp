import pytest
from aiohttp import web


async def test_text_response(hub):
    ret = await hub.server.web.text_response("Hello, world")
    assert isinstance(ret, web.Response)
    assert ret.status == 200
    assert ret.text == "Hello, world"
    assert ret.content_type == "text/plain"
    with pytest.raises(TypeError):
        await hub.server.web.text_response({"msg": "Hello, world"})


async def test_json_response(hub):
    ret = await hub.server.web.json_response({"msg": "Hello, world"})
    assert isinstance(ret, web.Response)
    assert ret.status == 200
    assert ret.text == '{"msg": "Hello, world"}'
    assert ret.content_type == "application/json"


async def test_response_handler(hub):
    ret = await hub.server.web.response_handler("Hello, world")
    assert isinstance(ret, web.Response)
    assert ret.status == 200
    assert ret.text == "Hello, world"
    assert ret.content_type == "text/plain"

    ret = await hub.server.web.response_handler({"msg": "Hello, world"})
    assert isinstance(ret, web.Response)
    assert ret.status == 200
    assert ret.text == '{"msg": "Hello, world"}'
    assert ret.content_type == "application/json"

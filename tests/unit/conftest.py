import aiohttp.web
import pytest


@pytest.fixture(scope="session", name="hub")
def unit_hub(hub):
    hub.pop.sub.add(dyne_name="acct")
    hub.pop.sub.add(dyne_name="log")
    hub.pop.sub.add(dyne_name="tool")
    hub.pop.sub.load_subdirs(hub.tool, recurse=True)
    hub.pop.sub.add(dyne_name="exec")
    hub.pop.sub.load_subdirs(hub.exec, recurse=True)
    hub.pop.sub.add(dyne_name="server")
    hub.pop.sub.load_subdirs(hub.server, recurse=True)
    yield hub


@pytest.fixture(scope="function", autouse=True)
async def cleanup_hub(hub):
    hub.tool.request.application.APP.freeze()
    await hub.tool.request.application.APP.shutdown()
    hub.tool.request.application.APP = aiohttp.web.Application()


@pytest.fixture(scope="session")
def acct_subs():
    return ["request"]


@pytest.fixture(scope="session")
def acct_profile():
    return "test_pop_aiohttp"

============================
pop-aiohttp Basic Web Server
============================

This plugin can start up an aiohttp web server, which can save some code
in your application and allow you to concentrate on your functions.

The basic setup is fairly easy, wherein you can set up routes and then pass
to ``hub.server.web.run`` coroutine which is run in the main asyncio loop.

Here's a code sample which can live inside your POP applications, init
function:


.. code-block:: python

    # Initialize the asyncio event loop
    hub.pop.loop.create()

    # Set up routes
    #   verb, location, hub function
    routes = [
        ["get", "/currentTime", hub.myapp.time_ops.get_current],
        ["get", "/randomNumber", hub.myapp.random_ops.get_number],
        ["get", "/", hub.myapp.text_ops.say_hello],
    ]

    # Start the async code
    coroutine = hub.server.web.run(routes=routes)
    hub.pop.Loop.run_until_complete(coroutine)


Each route is a list of between 1 and 3 items:

#. Verb (GET, POST, PUT, etc.)
#. Location (the URL path to route, such as "/" or "/foo")
#. Function (the hub function from your application that will handle the route)

If the length of the route list is only 2 items, we assume that the VERB is
missing and default to GET.

If the length of the route is only 1 item, we assume that both VERB and LOCATION
are missing and default to GET and "/" respectively.

Hub functions referenced by the route MUST return a ``web.Response`` object.
A helper function is provided at ``hub.server.web.response_handler`` on the hub.


.. code-block:: python

    async def say_hello(hub, request):
        some_message = "Hello!"
        return await hub.server.web.response_handler(some_message)


This function will automatically detect content type and provide text or JSON
responses to app clients.
